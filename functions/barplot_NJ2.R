
library(ggplot2)
library(gridExtra)

barplotNJ<-function(RawDatabaseTMP,output){
  

a <- ggplot(RawDatabaseTMP, aes(Lignee,Sphericity,fill=Lignee)) + 
  geom_boxplot(alpha = .4,outlier.shape = NA) +  labs(x="Type Cell", y = "Sphericity")




b <- ggplot(RawDatabaseTMP, aes(Lignee,SurfaceArea,fill=Lignee)) + 
  geom_boxplot(alpha = .4) +  labs(x="Type Cell", y = "Surface")


c <- ggplot(RawDatabaseTMP, aes(Lignee,Flatness,fill=Lignee)) + 
  geom_boxplot(alpha = .4) +  labs(x="Type Cell", y = "Flatness")


d<-ggplot(RawDatabaseTMP, aes(Lignee,Esr,fill=Lignee)) + 
  geom_boxplot(alpha = .4) +  labs(x="Type Cell", y = "ESR")

e<-ggplot(RawDatabaseTMP, aes(Lignee,Elongation,fill=Lignee)) + 
  geom_boxplot(alpha = .4) +  labs(x="Type Cell", y = "Elongation")

f<-ggplot(RawDatabaseTMP, aes(Lignee,Volume,fill=Lignee)) + 
  geom_boxplot(alpha = .4) +  labs(x="Type Cell", y = "Volume")

  plots.list = list(a, b, c, d,e,f)  # Make a list of plots
  
  pdf(paste(output,"plots_group.pdf",sep=""), onefile = TRUE)
  for (i in seq(length(plots.list))) {
    plot( plots.list[[i]])  
  }
  dev.off()

}


barplotNJ_WITH_FILTER<-function(RawDatabaseTMP,filterLignee,output){
  
  RawDatabaseTMP$Lignee <- factor(RawDatabaseTMP$Lignee,levels = filterLignee)
  a <- ggplot(RawDatabaseTMP, aes(Lignee,Sphericity,fill=Lignee)) + 
    geom_boxplot(alpha = .4,outlier.shape = NA) +  labs(x="Type Cell", y = "Sphericity")
  
  
  
  
  b <- ggplot(RawDatabaseTMP, aes(Lignee,SurfaceArea,fill=Lignee)) + 
    geom_boxplot(alpha = .4) +  labs(x="Type Cell", y = "Surface")
  
  
  c <- ggplot(RawDatabaseTMP, aes(Lignee,Flatness,fill=Lignee)) + 
    geom_boxplot(alpha = .4) +  labs(x="Type Cell", y = "Flatness")
  
  
  d<-ggplot(RawDatabaseTMP, aes(Lignee,Esr,fill=Lignee)) + 
    geom_boxplot(alpha = .4) +  labs(x="Type Cell", y = "ESR")
  
  e<-ggplot(RawDatabaseTMP, aes(Lignee,Elongation,fill=Lignee)) + 
    geom_boxplot(alpha = .4) +  labs(x="Type Cell", y = "Elongation")
  
  f<-ggplot(RawDatabaseTMP, aes(Lignee,Volume,fill=Lignee)) + 
    geom_boxplot(alpha = .4) +  labs(x="Type Cell", y = "Volume")
  
  
  
  
  plots.list = list(a, b, c, d,e,f)  # Make a list of plots
  
  pdf(paste(output,"plots_group.pdf",sep=""), onefile = TRUE)
  for (i in seq(length(plots.list))) {
    plot( plots.list[[i]])  
  }
  dev.off()
  
}


